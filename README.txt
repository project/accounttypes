/**
 * @package AccountTypes
 * @category NeighborForge
 */
--Accounttypes Module--

  This module is intended to make managing large sites with a lot of users and a lot of roles easier.
  Roles can be grouped into account types which the site admin defines. Each account type can use any
  combination of roles, so there can be some overlap of abilities/responsibilities if you want. The
  module starts out with an account type called 'basic' which cannot be deleted. It also starts out
  by having the 'authenticated user' role assigned to 'basic'. From there, you can add account types
  and roles to each as you see fit. You don't have to use the 'basic' account type if you don't want
  to. Simply uncheck any associated roles and it will be useless.
  
  You may also designate which of the assignable roles should automatically be assigned to the user
  when they receive the account type. This will not prevent you from removing roles from that user,
  but should minimize work on your part if you find that you are always assigning the same roles to
  users with the same account type.
  
  After setting up an account type and associating roles with it, you will find that on the user edit
  page, you can only assign the roles that correspond to that account type. All others are disabled
  and greyed out. Those roles designated as auto-assigned will become checked as you switch from one
  account type to another.
  
  If you have assigned the permission 'administer users' to someone, they will be able to assign both
  account types and roles from that type to users but will not be able to associate roles to account
  types, nor add new account types.

The pages you will need to visit to see all of the functionality are:

--site admin--
  admin/user/accounttypes -  this is where you define account types
  admin/user/accounttypes/edit - change the name of the account type or delete it
  admin/user/accounttypes/default - just a callback from the main page that sets which account type is
    automatically assigned to new site subscribers
  admin/user/accounttypes/admin - this is where you assign roles to the account types
  admin/user/accounttypes/assign_old_users - this is how to assign all pre-existing users to the default
    account type at once; see below for a one-by-one method to preserve existing roles

--user admin--
  admin/user/user - you can perform tasks on multiple users at a time and filter by account type
    note: because I hijacked the filter function (for lack of hooks), if another module does the same, one
    or the other won't work in all likelihood
  admin/user/user/create - add a user(as usual) and set their account type and roles
  user/X/edit - you edit a user here; looks similar to the user create page, but with fields added by
    other modules; by editing a pre-existing user, the account type can be set (previously this was broken)

--other--
  user/register - you won't see anything here as a user because the fields are hidden

I think that's everything. If I've forgotten something that you find, let me know.

As I'm new to Drupal and this was my first complete module, I'd be glad to know how to make it better,
particularly cleaning up any of the code to make it tighter.

Thanks. I hope you find this useful.